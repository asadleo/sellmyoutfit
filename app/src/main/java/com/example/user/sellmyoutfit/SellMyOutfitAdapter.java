package com.example.user.sellmyoutfit;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

public class SellMyOutfitAdapter extends RecyclerView.Adapter<SellMyOutfitViewHolder> {
    @NonNull
    @Override
    public SellMyOutfitViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SellMyOutfitViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull SellMyOutfitViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 8;
    }
}
