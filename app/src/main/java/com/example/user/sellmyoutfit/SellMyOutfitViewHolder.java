package com.example.user.sellmyoutfit;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class SellMyOutfitViewHolder extends RecyclerView.ViewHolder {

    public SellMyOutfitViewHolder(ViewGroup parent) {
        super(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row,parent,false));
    }
}
