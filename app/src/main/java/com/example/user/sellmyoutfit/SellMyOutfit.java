package com.example.user.sellmyoutfit;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;


/**
 * A simple {@link Fragment} subclass.
 */
public class SellMyOutfit extends BaseFragment {

    @BindView(R.id.rv)
    RecyclerView rv;
    private Context mContext;
    public SellMyOutfit() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_sell_my_outfit;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new GridLayoutManager(mContext,2));
        rv.setAdapter(new SellMyOutfitAdapter());
    }

}
