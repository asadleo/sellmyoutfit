package com.example.user.sellmyoutfit;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import butterknife.ButterKnife;

public abstract class CustomActivity extends AppCompatActivity {

    private static final boolean LOGGING = false; //false to disable logging

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResourceId());
        injectViews();
    }

    protected abstract int getLayoutResourceId();

    protected void hideStatusbar()
    {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    /**
     * Replace every field annotated with ButterKnife annotations like @BindView with the proper
     * value.
     */
    private void injectViews() {
        ButterKnife.bind(this);
    }


    protected static void d(String tag, String message) {
        if (LOGGING) {
            Log.d(tag, message);
        }
    }
}
