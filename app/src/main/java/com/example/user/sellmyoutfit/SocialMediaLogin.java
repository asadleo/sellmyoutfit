package com.example.user.sellmyoutfit;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class SocialMediaLogin extends CustomActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.findViewById(R.id.btn_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SocialMediaLogin.this,Login.class));
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        hideStatusbar();
        return R.layout.activity_login;
    }
}
